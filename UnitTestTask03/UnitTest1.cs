﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Task03;

namespace UnitTestTask03
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAccountsCountAndName()
        {
            //Arrange
            var mock = new Mock<IRepository<Account>>();
            mock.Setup(m => m.GetAll()).Returns(GetData());

            //Act

            //Assets            
            Assert.AreEqual(mock.Object.GetAll().ToArray().Length, 2);
            Assert.AreEqual(mock.Object.GetAll().First().FirstName, "TestName01");
        }

        public IEnumerable<Account> GetData()
        {
            List<Account> Accounts = new List<Account>()
            {
                new Account(){ FirstName="TestName01", LastName="TestLastName01", BirthDate = new DateTime(2003,6,23) },
                new Account(){ FirstName="TestName02", LastName="TestLastName02", BirthDate = new DateTime(2002,3,8) }
            };
            return Accounts;
        }


        [TestMethod]
        public void TestAddAccounts()
        {
            //Arrange
            //var mock = new Mock<IRepository<Account>>();            
            var repo = new Repository();

            //Act
            repo.Add(new Account() { FirstName = "TestName", LastName = "TestLastName", BirthDate = new DateTime(2000, 6, 23) });
            repo.Add(new Account() { FirstName = "TestName12", LastName = "TestLastName12", BirthDate = new DateTime(2001, 6, 23) });

            //Assets
            Assert.IsNotNull(repo, "Не Null");
            Assert.AreEqual(repo.GetAll().ToArray().Length, 2);
            Assert.AreEqual(repo.GetAll().Last().FirstName, "TestName12");
        }

    }
}
