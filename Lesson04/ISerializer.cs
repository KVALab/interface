﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections;
using System.IO;
using System.Xml;

namespace Lesson04
{
    interface ISerializer
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream stream);
    }


    class XmlSerializer : ISerializer
    {
        private static readonly XmlWriterSettings _xmlWriterSettings;

        static XmlSerializer()
        {
            _xmlWriterSettings = new XmlWriterSettings { Indent = true };
        }

        public string Serialize<T>(T item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
                .UseAutoFormatting()
                .UseOptimizedNamespaces()
                .EnableImplicitTyping(typeof(T))
                .Create();
            return serializer.Serialize(_xmlWriterSettings, item);
        }

        public T Deserialize<T>(Stream stream)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer().Create();
            return serializer.Deserialize<T>(stream);
        }
    }
}
