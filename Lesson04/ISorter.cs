﻿using System.Collections.Generic;

namespace Lesson04
{
    interface ISorter<T>
    {
        IEnumerable<T> Sort<T>(IEnumerable<T> Items); 
    }
}