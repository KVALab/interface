﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson04
{
    class Program
    {
        static void Main(string[] args)
        {
            ISerializer xmlSerializer = new XmlSerializer();

            //using (FileStream fs = new FileStream("data.txt", FileMode.OpenOrCreate, FileAccess.Write))
            //{
            //    using(StreamWriter sw = new StreamWriter(fs))
            //    {
            //        Random r = new Random();
            //        List<Person> list = new List<Person>();
            //        for (int i = 0; i < 10; i++)
            //            list.Add(new Person() { Name = $"Витя-{i}", SurName=$"Петров-{i}", Age=r.Next(18, 80) });

            //        //sw.Write( xmlSerializer.Serialize<Person[]>(list.ToArray()) );
            //    }
            //}


            OtusStreamReader<Person> osr;
            using ( FileStream fs = new FileStream("data.txt", FileMode.Open, FileAccess.Read) )
                osr = new OtusStreamReader<Person>(fs, xmlSerializer);

            osr.ForEach(p => Console.WriteLine(p));
            Console.WriteLine();

            PersonSorter ps = new PersonSorter();  //list.Sort(ps);
            ps.Sort(osr).ForEach(p => Console.WriteLine(p));

            Console.Read();
        }
    }
}
