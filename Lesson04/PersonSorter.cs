﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson04
{
    class PersonSorter : ISorter<Person>, IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return x.Age - y.Age;
        }

        public IEnumerable<T> Sort<T>(IEnumerable<T> Items)
        {
            var list = Items.ToList();
            list.Sort();
            return list;
        }
    }
}
