﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson04
{
    class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly ISerializer _serializer;
        private readonly Stream _stream;
        private readonly T[] _arr;

        public OtusStreamReader( Stream stream, ISerializer serializer)
        {
            _serializer = serializer;
            _stream = stream;
            _arr = _serializer.Deserialize<T[]>(_stream);
        }

        public IEnumerator<T> GetEnumerator()
        {            
            for (int i = 0; i < _arr.Length; i++)
            {
                yield return _arr[i];
            }
        }

        public void Dispose() { }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
