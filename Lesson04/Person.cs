﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson04
{
    class Person: IComparable<Person>
    {
        public string Name { get; set; }
        public string SurName { get; set; }
        public int Age { get; set; }

        public int CompareTo(Person obj)
        {
            return this.Age - obj.Age;
        }
        
        public override string ToString()
        {
            return $"Name-{Name}, SurName-{SurName}, Age-{Age}";
        }
    }
}
