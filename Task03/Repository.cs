﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03
{
    public class Repository : IRepository<Account>
    {
        private List<Account> _listAccounts = new List<Account>();

        /// <summary>
        /// Добавить аккаунт
        /// </summary>
        public void Add(Account item)
        {
            if (IsUniqueName(item.FirstName))
                _listAccounts.Add(item);
            else throw new ArgumentException("Ошибка добавления аккаунта, елемент с таким именем уже есть в коллекции", item.FirstName);
        }

        /// <summary>
        /// Проверить что имя уникальное в коллекции
        /// </summary>
        private bool IsUniqueName(string name)
        {            
            return GetOne(n => n.FirstName == name) is null;
        }
        
        /// <summary>
        /// Получить аккаунт согласно условию
        /// </summary>
        public Account GetOne(Func<Account, bool> predicate)
        {
            return _listAccounts.FirstOrDefault(predicate);            
        }

        public IEnumerable<Account> GetAll()
        {
            for (int i = 0; i < _listAccounts.Count; i++)
            {
                yield return _listAccounts[i];
            }
        }

    }
}
