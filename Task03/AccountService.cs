﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03
{
    public class AccountService : IAccountService
    {
        #region Поля
        private readonly IRepository<Account> _repository;
        private const int AGE18 = 18;
        #endregion

        public AccountService(IRepository<Account> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Добавление аккаунта в репозиторий после проверки на валидность
        /// </summary>        
        public void AddAccount(Account account)
        {
            if (ValidateAccount(ref account))
                _repository.Add(account);
            else throw new ArgumentException("Ошибка валидации данных при добавлении аккаунта!");
        }

        /// <summary>
        /// Проверить валидность данных аккаунта
        /// </summary>        
        private bool ValidateAccount(ref Account account)
        {
            return ! (string.IsNullOrEmpty(account.FirstName) ||
                      string.IsNullOrEmpty(account.LastName) ||
                      (DateTime.Now.Year - account.BirthDate.Year) < AGE18) ;
        }
    }
}
