﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03
{
    class Program
    {
        static void Main(string[] args)
        {
            //Список аккаунтов
            List<Account> listAccounts = new List<Account>()
            {
                new Account(){ FirstName="Name01", LastName="LastName01", BirthDate = new DateTime(2003,6,23) },
                new Account(){ FirstName="Name02", LastName="LastName02", BirthDate = new DateTime(2002,3,8) },
                new Account(){ FirstName="Name03", LastName="LastName03", BirthDate = new DateTime(2004,8,3) }
            };

            //Заполнить репозиторий аккаунтамии
            Repository repositoriy = new Repository();
            foreach (var account in listAccounts)
            {
                repositoriy.Add(account);
            }
            Console.WriteLine("Исходные данные в репозитории:");
            //Вывести данные репозитория
            repositoriy.GetAll().ToList().ForEach(a => Console.WriteLine(a));
            Console.WriteLine();

            //Создать аккаунт, добавить его в репозиторий через AccountService            
            AccountService accountService = new AccountService(repositoriy);
            try
            {
                accountService.AddAccount(new Account()
                {
                    FirstName = "NewName01",
                    LastName = "NewLastName01",
                    BirthDate = new DateTime(2000, 6, 23)
                });
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message + "\n");
            }

            Console.WriteLine("Данные в репозитории после добавления нового:");
            //Вывести данные репозитория после добавления
            foreach (var item in repositoriy.GetAll())
            { 
                Console.WriteLine(item); 
            }

            Console.Read();
        }
    }
}
